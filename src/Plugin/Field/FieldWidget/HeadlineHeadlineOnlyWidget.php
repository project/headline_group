<?php

namespace Drupal\headline_group\Plugin\Field\FieldWidget;

/**
 * Plugin implementation of the 'headline_headline_only' widget.
 *
 * @FieldWidget(
 *   id = "headline_headline_only",
 *   label = "Headline only",
 *   field_types = {
 *     "headline_group"
 *   }
 * )
 */
class HeadlineHeadlineOnlyWidget extends BaseHeadlineWidget {}
